window.onload = () => {
    // 1.1 Añade un botón a tu html con el id btnToClick y en tu javascript añade el evento click que ejecute un console log con la información del evento del click
    const body = document.querySelector('body');
    const btnJS = document.createElement('button');
    btnJS.id = 'btnToClick';
    btnJS.innerText = 'Boton con JS';
    body.appendChild(btnJS);
    // 1.2 Añade un evento 'focus' que ejecute un console.log con el valor del input.
    // 1.3 Añade un evento 'input' que ejecute un console.log con el valor del input.
    addEventListeners();
};

function addEventListeners() {
    // 1.1
    const inClick = document.querySelector('.click');
    const inFocus = document.querySelector('.focus');
    const inValue = document.querySelector('.value');
    const btnJS = document.querySelector('#btnToClick');
    btnJS.addEventListener('click', () => {
        console.log(inClick.value);
    });
    // 1.2
    inFocus.addEventListener('focus', () => {
        console.log(inFocus.value);
    });
    // 1.3
    inValue.addEventListener('input', () => {
        console.log(inValue.value);
    });
}
